﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace ConversionDataMapper
{
    class Program
    {
        public static void Main(string[] args)
        {
            // Wipe tables
            Console.WriteLine("Cleaning tables");
            ExecuteStatement(args[0], "EXEC [dbo].[usp_DeleteTables]");

            // Load metadata
            Console.WriteLine("Getting metadata");
            var metadata = QueryAll<ConversionMetadata>(args[0]);

            // Generate insert statements per destination table
            var tables = metadata.GroupBy(x => new { x.DestinationTableName, x.DestinationSchema, x.SourceTable });

            var sourceDb = "";
            var destinationDb = "";

            if (args.Length > 1 && string.IsNullOrEmpty(args[1]))
                sourceDb = $"{args[1]}.";

            if (args.Length > 2 && string.IsNullOrEmpty(args[2]))
                destinationDb = $"{args[2]}.";

            foreach (var table in tables)
            {
                try
                {
                    Console.WriteLine($"{table.Key.DestinationSchema}.{table.Key.DestinationTableName}, from {table.Key.SourceTable}");

                    var sourceTable = table.Key.SourceTable;
                    var destinationSchema = table.Key.DestinationSchema;
                    var destinationTable = table.Key.DestinationTableName;

                    // Build field list
                    var destinationFields = table.Select(x => x.DestinationColumnName).ToList();
                    var sourceFields = table.Select(x => x.SourceFieldName).ToList();

                    // Get natural keys (if there are any) 
                    var naturalKey = table.FirstOrDefault(x => !string.IsNullOrEmpty(x.DestinationNaturalKey));

                    if (naturalKey != null)
                    {
                        var destinationNaturalKeys = naturalKey.DestinationNaturalKey.Split(',').Where(x => !destinationFields.Contains(x)).ToList();
                        var sourceNaturalKeys = naturalKey.SourceNaturalKey.Split(',').Where(x => !sourceFields.Contains(x)).ToList();

                        destinationFields = destinationFields.Concat(destinationNaturalKeys ?? Enumerable.Empty<string>()).ToList();
                        sourceFields = sourceFields.Concat(sourceNaturalKeys ?? Enumerable.Empty<string>()).ToList();
                    }

                    var stmt = @$"INSERT INTO {destinationDb}[{destinationSchema}].[{destinationTable}]({GetFieldsForStatement(destinationFields)})
                        SELECT {GetFieldsForStatement(sourceFields)}
                        FROM {sourceDb}[cnv].[{sourceTable}]
                        WHERE HasValidationIssues = 0";

                    ExecuteStatement(args[0], stmt);
                }
                catch (Exception e)
                {
                    Console.WriteLine($"Error: {e.Message}");
                }
            }

            InsertIdentifiers(args[0]);
        }

        public static string GetFieldsForStatement(IEnumerable<string> list)
        {
            return list.Aggregate("", (result, next) => $"{result},[{next}]", result => result.Substring(1));
        }

        public static void InsertIdentifiers(string connexionString)
        {
            var stmt = @";WITH PropertyCollateral AS (
                                    	SELECT * FROM [DEV_IT_FMOL].[cnv].[PayloadIdentifier]
                                    	WHERE LoanIdentifier IS NULL AND FannieMaeLoanNumber IS NULL
                                    ), Deals As (
                                    	SELECT * FROM [DEV_IT_FMOL].[cnv].[PayloadIdentifier]
                                    	WHERE DealIdentifier IS NOT NULL
                                    )
                                    INSERT INTO [trf].[PayloadIdentifier]([PayloadId],[PayloadType],[DealIdentifier],[DealCollateralRecourseIdentifier],[LoanIdentifier],[FannieMaeLoanNumber],[CollateralReferenceNumber],[PropertyIdentifier])
                                    SELECT 
                                    [PayloadId] = 0
                                    ,[PayloadType] = '00'
                                    ,D.[DealIdentifier]
                                    ,D.[DealCollateralRecourseIdentifier]
                                    ,L.[LoanIdentifier]
                                    ,L.[FannieMaeLoanNumber]
                                    ,L.[CollateralReferenceNumber]
                                    ,C.[PropertyIdentifier]
                                    FROM [DEV_IT_FMOL].[cnv].[PayloadIdentifier] L
                                    LEFT JOIN Deals D ON L.[LoanIdentifier] = D.[LoanIdentifier] AND L.[FannieMaeLoanNumber] = D.[FannieMaeLoanNumber]
                                    LEFT JOIN PropertyCollateral C ON C.[CollateralReferenceNumber] = L.[CollateralReferenceNumber]
                                    WHERE (L.LoanIdentifier IS NOT NULL OR L.FannieMaeLoanNumber IS NOT NULL) AND L.DealIdentifier IS NULL";

            ExecuteStatement(connexionString, stmt);
        }

        public static IList<T> QueryAll<T>(string connexionString)
        {
            using (var cnx = NewConnection(connexionString))
            {
                var excludedFiles = new string[]{ "Staging_PropertyFinancialsComment",
                    "Staging_LoanRiskMonitoringComment",
                    "Staging_PropertyInspectionsComment", "Staging_AssetRiskRatingComment" };

                using (var db = new Database(cnx)) 
                    return db.Fetch<T>("WHERE DestinationTableName IS NOT NULL AND [SourceTableName] NOT IN (@0)", excludedFiles);
            }
        }

        public static void ExecuteStatement(string connexionString, string sqlStmt, params object[] args)
        {
            using (var cnx = NewConnection(connexionString))
            {
                using (var db = new Database(cnx))
                {
                    db.CommandTimeout = 120;
                    db.Execute(sqlStmt, args);
                }
            }
        }

        public static SqlConnection NewConnection(string connexionString, bool autoOpen = true)
        {
            var cnx = new SqlConnection(connexionString);
            if (autoOpen)
                cnx.Open();
            return cnx;
        }

        [TableName("[cnv].[ConversionMetadata]")]
        public class ConversionMetadata
        {
            public int Id { get; set; }
            [Column("SourceTableName")]
            public string SourceTable { get; set; }
            [Column("FieldName")]
            public string SourceFieldName { get; set; }
            public string SourceNaturalKey { get; set; }
            [Column("SchemaName")]
            public string DestinationSchema { get; set; }
            public string DestinationTableName { get; set; }
            public string DestinationColumnName { get; set; }
            public string DestinationNaturalKey { get; set; }
        }
    }
}